import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {NutrientFact, Product, Recipe} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

recipeData.forEach(data => {
    recipeSummary[data.recipeName] = getCheapest(data)
})

// Get the cheapest cost and combined nutrifacts
function getCheapest(data: Recipe) {
    let recipe = {
        cheapestCost: 0,
        nutrientsAtCheapestCost: {}
    };

    // Iterate through lineItems
    data.lineItems.forEach(lineItem => {
        // Return an object with cost per base unit and nutrient fact in base units
        const cheapestIngredient = GetProductsForIngredient(lineItem.ingredient).map((product: Product) => {
                    const costPerBaseUnits = product.supplierProducts.map(cost => GetCostPerBaseUnit(cost));
                    const nutrientFactInBaseUnits = product.nutrientFacts.map(facts => GetNutrientFactInBaseUnits(facts));
                    return {costPerBaseUnitCheapest: Math.min(...costPerBaseUnits),nutrientFactInBaseUnits: nutrientFactInBaseUnits};
                })
                // Find the cheapest cost
                .reduce((acc, c) => c.costPerBaseUnitCheapest < acc.costPerBaseUnitCheapest ? c : acc);

        // Iterate through the cheapest ingredients then add the quantity amount
        cheapestIngredient.nutrientFactInBaseUnits.forEach((nutrient: NutrientFact) => {
            const nutrientName = nutrient.nutrientName;
            if (!recipe.nutrientsAtCheapestCost.hasOwnProperty(nutrientName)){
                recipe.nutrientsAtCheapestCost[nutrientName] = nutrient;
            }else{
                recipe.nutrientsAtCheapestCost[nutrientName].quantityAmount.uomAmount += nutrient.quantityAmount.uomAmount;
            }
        });

        // Add value to the cheapest cost
        recipe.cheapestCost += cheapestIngredient.costPerBaseUnitCheapest * (lineItem.unitOfMeasure.uomAmount);
        // sort the nutrifacts alphabetically
        recipe.nutrientsAtCheapestCost = Object.keys(recipe.nutrientsAtCheapestCost).sort()
            .reduce(function (result, key) {
                result[key] = recipe.nutrientsAtCheapestCost[key]
                return result
            }, {});

    });
    return recipe;
}

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
